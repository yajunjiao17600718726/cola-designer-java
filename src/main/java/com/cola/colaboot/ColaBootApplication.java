package com.cola.colaboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.cola.colaboot.module.*.mapper")
public class ColaBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(ColaBootApplication.class, args);
    }

}
