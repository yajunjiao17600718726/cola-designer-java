package com.cola.colaboot.module.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cola.colaboot.module.system.pojo.SysAccess;

import java.util.List;

public interface SysAccessService extends IService<SysAccess> {
    List<SysAccess> getAccessByUserName(String username);
    List<SysAccess> listRoleAccessByParent(SysAccess access);

    List<SysAccess> getChildren(String parentId);

    void doSave(SysAccess access);

    void deleteNative(String id);

    List<SysAccess> treeRoleAccess(Integer menuType);

    List<Integer> getAccessIdsByRole(Integer roleId);
}
