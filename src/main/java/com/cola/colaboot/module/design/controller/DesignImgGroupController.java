package com.cola.colaboot.module.design.controller;

import com.cola.colaboot.config.dto.Res;
import com.cola.colaboot.module.design.pojo.DesignImgGroup;
import com.cola.colaboot.module.design.service.DesignImgGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/imgGroup")
public class DesignImgGroupController {

    @Autowired
    private DesignImgGroupService groupService;

    @GetMapping("/pageList")
    public Res<?> queryPageList(DesignImgGroup group,
                                @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                                @RequestParam(name="pageSize", defaultValue="10") Integer pageSize) {
        return Res.ok(groupService.pageList(group,pageNo,pageSize));
    }

    @GetMapping("/listAll")
    public Res<?> listAll() {
        return Res.ok(groupService.list());
    }

    @PostMapping("/saveOrUpdate")
    public Res<?> saveOrUpdate(@RequestBody DesignImgGroup group){
        groupService.saveOrUpdate(group);
        return Res.ok();
    }
}
