package com.cola.colaboot.module.design.controller;

import com.cola.colaboot.config.dto.Res;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping("/textData")
    public Res<?> textData(){
        return Res.ok("success","我是文本组件API测试数据");
    }
}
